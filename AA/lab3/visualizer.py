#! /bin/python

import sys
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt

rc('text', usetex=True)
f = sys.argv[1]
data = np.genfromtxt(f, delimiter=',', names=['n', 'pred3', 'pred9', 'pred30', 'pred100'], skip_header=1)

fig, ax = plt.subplots()
ax.plot(data['n'], data['pred3'], color='r', marker='.', linestyle='', label='m=3')
ax.plot(data['n'], data['pred9'], color='g', marker='.', linestyle='', label='m=9')
ax.plot(data['n'], data['pred30'], color='b', marker='.', linestyle='', label='m=30')
ax.plot(data['n'], data['pred100'], color='m', marker='.', linestyle='', label='m=100')
ax.set_title(sys.argv[2])
ax.legend()

plt.show()
