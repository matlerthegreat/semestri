package main

import (
    "fmt"
    "math"
    "math/rand"
    "crypto/md5"
    )

/*func h(x int64) float64 {
    rand.Seed(x)
    return rand.Float64()
}*/

func h(x int64) float64 {
    b := 64
    hash := md5.Sum([]byte(fmt.Sprintf("%v", x)))
    full_hash := uint64(hash[0]) + uint64(hash[1])<<8 + uint64(hash[2])<<16 + uint64(hash[3])<<24 + uint64(hash[4])<<32 + uint64(hash[5])<<40 + uint64(hash[6])<<48 + uint64(hash[7])<<56
    reduced_hash := (full_hash << (64-b)) >> (64-b)

    h := float64(reduced_hash)/math.Pow(2.0, float64(b))
    //bad_hash := math.Sin((h*2-1)*math.Pi/2)/2+0.5
    //return bad_hash
    return h
}


const a = 1.0
const b = 100.0
var M3 []float64 = make([]float64, 3)
var M9 []float64 = make([]float64, 9)
var M30 []float64 = make([]float64, 30)
var M100 []float64 = make([]float64, 100)

var N3 []float64 = make([]float64, 3)
var N9 []float64 = make([]float64, 9)
var N30 []float64 = make([]float64, 30)
var N100 []float64 = make([]float64, 100)

func Count(M []float64, N []float64, m int, i int, lambda float64) {
    for k := 0; k < m; k++ {
        u := h(int64(i)<<32 + int64(k))

        v := -math.Log(u)
        if (v/lambda) < M[k] {
            M[k] = v/lambda
        }
        if v < N[k] {
            N[k] = v
        }
    }
}

func GetCount(M []float64, N []float64, m int) float64 {
    sum1 := 0.0
    sum2 := 0.0
    for k := 0; k < m; k++ {
        sum1 += M[k]
        sum2 += N[k]
    }
    return sum2/sum1
}


func main() {
    c := 0

    fmt.Printf("n, pred3, pred9, pred30, pred100\n")
    for n := 0; n < 1000; n++ {
        for k := 0; k < 3; k++ {
            M3[k] = math.Inf(1)
            N3[k] = math.Inf(1)
        }
        for k := 0; k < 9; k++ {
            M9[k] = math.Inf(1)
            N9[k] = math.Inf(1)
        }
        for k := 0; k < 30; k++ {
            M30[k] = math.Inf(1)
            N30[k] = math.Inf(1)
        }
        for k := 0; k < 100; k++ {
            M100[k] = math.Inf(1)
            N100[k] = math.Inf(1)
        }

        sum := 0.0

        for i := 0; i < n; i++ {
            lambda := rand.Float64() * (b-a) + a
            sum += lambda
            Count(M3, N3, 3, c, lambda)
            Count(M9, N9, 9, c, lambda)
            Count(M30, N30, 30, c, lambda)
            Count(M100, N100, 100, c, lambda)
            c++
        }

        sum /= float64(n)

		pred3:= GetCount(M3, N3, 3) / sum
		pred9 := GetCount(M9, N9, 9) / sum
		pred30 := GetCount(M30, N30, 30) / sum
		pred100 := GetCount(M100, N100, 100) / sum

    	fmt.Printf("%v, %f, %f, %f, %f\n", n, pred3, pred9, pred30, pred100)
    }
}
