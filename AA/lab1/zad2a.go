package main

import (
    "fmt"
    "time"
    "math/rand"
    "./Simulations"
    )

func main() {
    rand.Seed(time.Now().UnixNano())

	fmt.Printf("n, time\n")
	samples_count := 100
    for n := 2; n <= 100; n++ {
        p := func (t int) bool {
            p := 1.0/float64(n)

            if (rand.Float64() <= p) {
                return true
            } else {
                return false
            }
        }


		var time_sum = 0
        for i := 0; i < samples_count; i++ {
            time_sum = time_sum + Simulation.Simulate(n, p)
        }
        time_average := float64(time_sum)/float64(samples_count)
    	fmt.Printf("%v, %v\n", n, time_average)
    }
}
