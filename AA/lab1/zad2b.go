package main

import (
    "fmt"
    "time"
    "math"
    "math/rand"
    "./Simulations"
    )

func main() {
    rand.Seed(time.Now().UnixNano())

	fmt.Printf("n, u, time\n")
	samples_count := 100
    for u := 3; u <= 100; u++ {
        L := int(math.Ceil(math.Log2(float64(u))))
        p := func (t int) bool {
            i := t % L
            p := math.Pow(0.5, float64(i))

            if (rand.Float64() <= p) {
                return true
            } else {
                return false
            }
        }


		n := u
		var time_sum = 0
        for i := 0; i < samples_count; i++ {
            time_sum = time_sum + Simulation.Simulate(n, p)
        }
        time_average := float64(time_sum)/float64(samples_count)
    	fmt.Printf("%v, %v, %v\n", n, u, time_average)
    }
}
