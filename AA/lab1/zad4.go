package main

import (
    "fmt"
    "time"
    "math"
    "math/rand"
    "./Simulations"
    )

func main() {
    rand.Seed(time.Now().UnixNano())

	fmt.Printf("n, u, Pr\n")
	samples_count := 100
    for u := 3; u <= 100; u = u * 2 {
        L := int(math.Ceil(math.Log2(float64(u))))
        p := func (t int) bool {
            i := t % L
            p := math.Pow(0.5, float64(i))

            if (rand.Float64() <= p) {
                return true
            } else {
                return false
            }
        }

		for n := 2; n < u; n = n * 2 {
    		var success_count = 0
            for i := 0; i < samples_count; i++ {
                time := Simulation.Simulate(n, p)
                if time <= L {
                    success_count = success_count + 1
                }
            }
            success_average := float64(success_count)/float64(samples_count)
        	fmt.Printf("%v, %v, %v\n", n, u, success_average)
		}
    }
}
