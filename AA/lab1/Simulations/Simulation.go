package Simulation

import (
    "log"
)

const (
    NULL	= iota
    SINGLE
	COLL
)

type Probability func(t int) bool

func Node(id int, p Probability, time chan int, c chan int) {
    for t := range time {
        if (p(t)) {
            log.Printf("Node %v speaking...\n", id)
            c <- id
        } else {
            c <- 0
        }
    }
}

func TickNodes(connections []chan int, t int) {
    for i, _ := range connections {
        connections[i] <- t
    }
}

func Simulate(n int, p Probability) int {
    connections := make([]chan int, n)
    messages := make(chan int)
    for i := 0; i < n; i++ {
        connections[i] = make(chan int, 1)
        go Node(i + 1, p, connections[i], messages)
    }

	var t int = 0
	var state int = NULL
	var leader int = -1
	for {
    	t++
        log.Printf("### Tick %v!\n", t)

    	state = NULL
		go TickNodes(connections, t)

		for i := 0; i < n; i++ {
    		m := <-messages
    		if m != 0 {
    			switch state {
        			case NULL:
            			state = SINGLE
    					leader = m
    				default:
        				state = COLL
        				leader = -1
    			}
    		}
		}

		if (state == SINGLE) {
    		break
		}
	}

    log.Printf("The leader after %v ticks is %v!\n", t, leader)
    return t
}
