package main

import (
    "fmt"
    "time"
    "math/rand"
    "./Simulations"
    )

func main() {
    rand.Seed(time.Now().UnixNano())

	fmt.Printf("Ex, Var\n")
	samples_count := 10000
	var l int = 0
	var ls int = 0

	n := 100

    p := func (t int) bool {
        p := 1.0/float64(n)

        if (rand.Float64() <= p) {
            return true
        } else {
            return false
        }
    }

    for i := 0; i < samples_count; i++ {
        t := Simulation.Simulate(n, p)
        l = l + t
        ls = ls + (t*t)
    }
    Ex := float64(l)/float64(samples_count)
    Var := (float64(ls)/float64(samples_count)) - (Ex*Ex)

	fmt.Printf("%v, %v\n", Ex, Var)
}
