package main

import (
    "fmt"
    "math"
    "math/bits"
    "math/rand"
    )

func h(x int) uint32 {
    rand.Seed(int64(x))
    return uint32(rand.Int63())
}

func Zeros(m []int) int {
    var zeros int = 0
    for _, x := range(m) {
        if (x == 0) {
            zeros++
        }
    }

    return zeros
}

func FirstOne(x uint32) int {
    return bits.LeadingZeros32(x) + 1
}

func Max(x, y int) int {
    if x > y {
        return x
    }
    return y
}

func Alpha(m int) float64 {
    if (m == 16) {
        return 0.673
    } else if (m == 32) {
        return 0.697
    } else if (m == 64) {
        return 0.709
    }
    return 0.7213/(1+1.079/float64(m))
}

func HyperLogLog(Em map[int]int, b int) float64 {
    m := 1<<b
    M := make([]int, m)

    for v, _ := range(Em) {
        x := h(v)
        j := x>>(32-b)
        w := x<<b | 1<<(b-1)
        M[j] = Max(M[j], FirstOne(w))
    }

    var sum float64 = 0
    for i := 0; i < m; i++ {
        sum += math.Pow(2, -float64(M[i]))
    }
    E := Alpha(m)*math.Pow(float64(m), 2)/sum

    if (E < 5/2*float64(m)) {
        V := Zeros(M)
        if V != 0 {
            return float64(m)*math.Log(float64(m)/float64(V))
        } else {
            return E
        }
    } else if (E < 1/30*float64(1<<32)) {
        return E
    }

    return -float64(1<<32)*math.Log(1-E/float64(1<<32))
}

func main() {
    b := 16
    fmt.Printf("n, pred\n")
    for n := 1; n < 10000; n++ {
        Em := make(map[int]int)
        for i := 0; i < n; i++ {
            x := rand.Int()
            Em[x] = 1
        }
        E := HyperLogLog(Em, b)
        pred := E/float64(n)
    	fmt.Printf("%v, %f\n", n,  pred)
    }
}
