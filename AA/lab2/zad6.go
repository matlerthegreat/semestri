package main

import (
    "fmt"
    "sort"
    "math"
    "math/rand"
    "crypto/md5"
)

func IsIn(x float64, ys []float64) bool {
    for _, y := range ys {
        if x == y {
            return true
        }
    }
    return false
}

func CountOtherThanOne(xs []float64) int {
    count := 0
    for _, x := range xs {
        if x != 1 {
            count++
        } else {
            return count
        }
    }
    return count
}

func h(x int) float64 {
    b := 8
    hash := md5.Sum([]byte(fmt.Sprintf("%v", x)))
    full_hash := uint32(hash[0]) + uint32(hash[1])<<8 + uint32(hash[2])<<16 + uint32(hash[3])<<24
    reduced_hash := (full_hash << (32-b)) >> (32-b)

    h := float64(reduced_hash)/float64(uint64(1)<<b)
    bad_hash := math.Sin((h*2-1)*math.Pi/2)/2+0.5
    return bad_hash
}

func MinCount(Em map[int]int, k int) int {
    M := make([]float64, k)

    for i := 0; i < k; i++ {
        M[i] = 1
    }

    for v := range(Em) {
        h := h(v)
        if h < M[k-1] && !IsIn(h, M) {
            M[k-1] = h
            sort.Float64s(M)
        }
    }

    if M[k-1] == 1 {
        return CountOtherThanOne(M)
    }
    return int(float64(k-1)/M[k-1])
}

func main() {
    k := 400
    fmt.Printf("n, pred\n")
    for n := 1; n < 10000; n++ {
        Em := make(map[int]int)
        for i := 0; i < n; i++ {
            x := rand.Int()
            Em[x] = 1
        }
        E := MinCount(Em, k)
        pred := float64(E)/float64(n)
    	fmt.Printf("%v, %f\n", n,  pred)
    }
}
