package main

import (
    "fmt"
    "sort"
    "math/rand"
)

func IsIn(x float64, ys []float64) bool {
    for _, y := range ys {
        if x == y {
            return true
        }
    }
    return false
}

func CountOtherThanOne(xs []float64) int {
    count := 0
    for _, x := range xs {
        if x != 1 {
            count++
        } else {
            return count
        }
    }
    return count
}

func h(x int) float64 {
    rand.Seed(int64(x))
    return float64(rand.Float64())
}

func MinCount(Em map[int]int, k int) int {
    M := make([]float64, k)

    for i := 0; i < k; i++ {
        M[i] = 1
    }

    for v := range(Em) {
        h := h(v)
        if h < M[k-1] && !IsIn(h, M) {
            M[k-1] = h
            sort.Float64s(M)
        }
    }

    if M[k-1] == 1 {
        return CountOtherThanOne(M)
    }
    return int(float64(k-1)/M[k-1])
}

func main() {
    k := 2
    fmt.Printf("n, pred\n")
    for n := 1; n < 10000; n++ {
        Em := make(map[int]int)
        for i := 0; i < n; i++ {
            x := rand.Int()
            Em[x] = 1
        }
        E := MinCount(Em, k)
        pred := float64(E)/float64(n)
    	fmt.Printf("%v, %f\n", n,  pred)
    }
}
