#! /bin/python

import sys
import math
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt

def grunspan(n, q):
    p = 1-q

    sum = np.zeros(np.shape(q))
    for k in range(n):
        l = math.factorial(k+n-1)/math.factorial(k)/math.factorial(n-1)
        sum += (p**n * q**k - q**n * p**k) * l
    return 1 - sum


rc('text', usetex=True)
n = sys.argv[1]
file_name = "resultn" + n + ".csv"
data = np.genfromtxt(file_name, delimiter=',', names=['q', 'success_rate'], skip_header=1)

q = np.linspace(0.01, 0.5, 500)

fig, ax = plt.subplots()
ax.plot(q, grunspan(int(n), q), color='b', marker='.', linestyle='', label='calculated')
ax.plot(data['q'], data['success_rate'], color='r', marker='.', linestyle='', label='simulated')
ax.set_title("n = " + n)
ax.legend()

plt.show()
