package main

import (
    "os"
    "fmt"
    "strconv"
    "math/rand"
    )

const samples_count = 1000
const rounds_count = 1000
const total_computation_count = 1000.0

func CalculateBlock(calculations_count float64) int{
    for i := 0.0; i < calculations_count; i++ {
        h := rand.Float64()
        if h < (1.0 / total_computation_count) {
            return 1
        }
    }
    return 0
}

func AttackBlockchain(n int, q float64) bool {
    adversary_length := 0
    genuine_length := 0

    adversary_power := total_computation_count * q
    genuine_power := total_computation_count - adversary_power
    for round := 0; round < n; round++ {
        adversary_length += CalculateBlock(adversary_power)
        genuine_length += CalculateBlock(genuine_power)
    }
    for round := n; round < rounds_count; round++ {
        adversary_length += CalculateBlock(adversary_power)
        genuine_length += CalculateBlock(genuine_power)

        if adversary_length >= genuine_length {
            return true
        }
    }

    return false
}

func main() {
    n, _ := strconv.Atoi(os.Args[1])
    fmt.Printf("q, success_rate\n")
    for q:= 0.01; q <= 0.5; q+= 0.01 {
        success_count := 0
        for sample := 0; sample < samples_count; sample++ {
            if AttackBlockchain(n, q) {
                success_count++
            }
        }

        success_rate := float64(success_count)/float64(samples_count)
        fmt.Printf("%f, %f\n", q, success_rate)
    }
}
