#! /bin/python

import sys
import math
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt

rc('text', usetex=True)

def nakamato(n, q):
    p = 1-q
    l = n*q/p

    sum = np.zeros(np.shape(q), dtype="O")
    for k in range(n):
        qnk = (q/p)**(n-k)
        sum += l**k * (1-qnk) / math.factorial(k)
    return 1 - np.exp(-l) * sum

def grunspan(n, q):
    p = 1-q

    sum = np.zeros(np.shape(q))
    for k in range(n):
        l = math.factorial(k+n-1)/math.factorial(k)/math.factorial(n-1)
        sum += (p**n * q**k - q**n * p**k) * l
    return 1 - sum

def nakamato_p(p, q):
    ns = np.zeros(np.shape(q))
    for n in range(100):
        ns = np.where(nakamato(n, q) < p, ns, n)
    return ns

def grunspan_p(p, q):
    ns = np.zeros(np.shape(q))
    for n in range(100):
        ns = np.where(grunspan(n, q) < p, ns, n)
    return ns

q = np.linspace(0, 0.5, 1000)
p = float(sys.argv[1]) / 100

fig, ax = plt.subplots()

ax.plot(q, nakamato_p(p, q), color='r', marker='.', linestyle='', label='nakamato')
ax.plot(q, grunspan_p(p, q), color='b', marker='.', linestyle='', label='grunspan')
ax.set_title("$p <" + str(p) + "$")

ax.legend()

plt.show()
