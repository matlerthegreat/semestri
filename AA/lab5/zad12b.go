package main

import (
    "fmt"
    "sync"
    )

const n = 8
type State = [n]int
var all_valid_states map[State]bool
var mux sync.Mutex

func dijkstra(state State, pending_states *[]State, visited_states map[State]bool, valid_states map[State]bool) bool {
    visited_states[state] = true
    legal := true
    value := state[0]
    for i := 1; i < n; i++ {
        if (state[i] != value) {
            legal = false
			break
        }
    }
    if legal {
        valid_states[state] = true
        return true
    }

    for i := 0; i < n; i++ {
        new_state := state
        if (i == 0) {
            if (state[0] == state[n-1]) {
                new_state[0] = (state[0] + 1)%(n+1)
            }
        } else {
            if (state[i] != state[i-1]) {
                new_state[i] = state[i-1]
            }
        }

		if valid_states[new_state] {
    		return true
		}
		if !visited_states[new_state] {
            *pending_states = append(*pending_states, new_state)
		}
    }

    return false
}

func check_state(state State, valid_states map[State]bool) []State {
    var invalid_states []State
    var pending_states []State
	pending_states = append(pending_states, state)
	visited_states := make(map[State]bool)
	valid := false
	for len(pending_states) > 0 {
    	state_to_check := pending_states[0]
    	pending_states = pending_states[1:]

        if dijkstra(state_to_check, &pending_states, visited_states, valid_states) {
            valid = true
            break
        }
	}
	pending_states = nil
	if !valid {
    	invalid_states = append(invalid_states, state)
    	fmt.Println("Not valid!")
	}

	return invalid_states
}

func check_states(start_state State, end_state State, end_chan chan []State) {
    var invalid_states []State
    state := start_state
    fmt.Println("od ", start_state, " do ", end_state)

	valid_states := make(map[State]bool)
	step := 0
	for {
    	if (step % 1000 == 0) {
        	mux.Lock()
        	for valid_state, _ := range(all_valid_states) {
            	valid_states[valid_state] = true
        	}
        	for valid_state, _ := range(valid_states) {
            	all_valid_states[valid_state] = true
        	}
        	mux.Unlock()
    	}
    	//fmt.Println(state)
    	check_state(state, valid_states)

    	for i := 0; i < n; i++ {
        	if state[i] < n {
            	state[i] += 1
            	break
        	}
        	state[i] = 0
    	}

    	if state == end_state {
        	break
    	}
    	step += 1
	}

	end_chan <- invalid_states
}

func main() {
    all_valid_states = make(map[State]bool)
    end_chan := make(chan []State)
    started_checks := 0
    for i := 0; i < n; i++ {
       var start_state State
       var stop_state State
       start_state[n-1] = i
       stop_state[n-1] = (i+1)%n
       go check_states(start_state, stop_state, end_chan)
       started_checks += 1
    }

    for i := 0; i < started_checks; i++ {
    	fmt.Println("Sprawdzam")
        invalid_states := <-end_chan
    	for _, invalid_state := range(invalid_states) {
        	fmt.Println("Stan ", invalid_state, " jest nieprawidłowy!")
    	}
	}
}
