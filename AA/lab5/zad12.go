package main

import (
    "fmt"
    "time"
    )

func dijkstra_step(n int, conf []int, i int, step int, start chan int, stop chan int) {
    if (i == 0) {
        if (conf[0] == conf[n-1]) {
            conf[0] = (conf[0] + 1)%(n+1)
        }
    } else {
        if (conf[i] != conf[i-1]) {
            conf[i] = conf[i-1]
        }
    }

    legal := true
    value := conf[0]
    for j := 1; j < n; j++ {
        if (conf[j] != value) {
            legal = false
			break
        }
    }
    if legal {
        stop <- step
        return
    }

    for j := 0; j < n; j++ {
        start <- step+1
        new_conf := make([]int, n)
        copy(new_conf, conf)
    	go dijkstra_step(n, new_conf, j, step+1, start, stop)
	}
}

func dijkstra(n int, start chan int, stop chan int) {
	conf := make([]int, n)
	for i := 0; i < n; i++ {
    	conf[i] = 0
	}
    start <- 0
	for {
    	fmt.Println(conf)
        for j := 0; j < n; j++ {
            start <- 1
            new_conf := make([]int, n)
            copy(new_conf, conf)
        	go dijkstra_step(n, new_conf, j, 1, start, stop)
    	}
    	for j := 0; j < n; j++ {
        	if conf[j] < n {
            	conf[j] += 1
            	break
        	}
        	conf[j] = 0
    	}

    	finished := true
    	for j := 0; j < n; j++ {
        	if conf[j] !=  0 {
            	finished = false
            	break
        	}
    	}

    	if finished {
        	stop <- 0
        	return
    	}
	}
}

func main() {
    n := 3
	start := make(chan int)
	stop := make(chan int)

	go dijkstra(n, start, stop)

	started := 0
	finished := 0
	for {
    	select {
        	case <-stop:
                //fmt.Println("Stopping step ", step)
                finished += 1
                if (started == finished) {
                    fmt.Println("Finished!")
                    fmt.Printf("Started %v processes and finished %v\n", started, finished)
                    return;
                }
            default:
                time.Sleep(time.Millisecond )
    	}
    	select {
        	case <-start:
                //fmt.Println("Starting step ", step)
            	started += 1
            case <-stop:
                //fmt.Println("Stopping step ", step)
                finished += 1
                if (started == finished) {
                    fmt.Println("Finished!")
                    fmt.Printf("Started %v processes and finished %v\n", started, finished)
                    return;
                }
    	}
	}

}
