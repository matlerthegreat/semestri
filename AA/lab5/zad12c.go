package main

import (
    "fmt"
    "sync"
    )

const n = 2
type Config = [n]int
type State struct {
    config Config
    next_proc int
}

var all_valid_states map[Config]bool
var mux sync.Mutex

func dijkstra(trace *[]State) bool {
	state := (*trace)[len(*trace)-1]
    legal := true
    value := state.config[0]
    for i := 1; i < n; i++ {
        if (state.config[i] != value) {
            legal = false
			break
        }
    }
    if legal {
        *trace = (*trace)[:len(*trace)-1]
        return true
    }

    var new_state State
    new_state.config = state.config
    if state.next_proc == 0 {
        if (state.config[0] == state.config[n-1]) {
            new_state.config[0] = (state.config[0] + 1)%(n+1)
        }
    } else {
        if (state.config[state.next_proc] != state.config[state.next_proc-1]) {
            new_state.config[state.next_proc] = state.config[state.next_proc-1]
        }
    }

	var is_new bool
    for i := 0; i < n; i++ {
		is_new = true
    	for _, old_state := range(*trace) {
        	if old_state.config == new_state.config && old_state.next_proc == i {
            	is_new = false
            	break
        	}
    	}
    	if is_new {
            new_state.next_proc = i
            *trace = append(*trace, new_state)
            return true
    	}
	}

	return false
}

func check_state(config Config) bool {
    var trace []State
    trace = append(trace, State{config, 0})

    for len(trace) > 0 {
        if !dijkstra(&trace) {
            return false
        }
    }

	return true
}

func check_states(start_state Config, end_state Config, end_chan chan []Config) {
    var invalid_states []Config
    state := start_state
    fmt.Println("od ", start_state, " do ", end_state)

	step := 0
	for {
    	//fmt.Println(state)
    	if (!check_state(state)) {
        	invalid_states = append(invalid_states, state)
    	}

    	for i := 0; i < n; i++ {
        	if state[i] < n {
            	state[i] += 1
            	break
        	}
        	state[i] = 0
    	}

    	if state == end_state {
        	break
    	}
    	step += 1
	}

	end_chan <- invalid_states
}

func main() {
    all_valid_states = make(map[Config]bool)
    end_chan := make(chan []Config)
    started_checks := 0
    for i := 0; i < n; i++ {
       var start_state Config
       var stop_state Config
       start_state[n-1] = i
       stop_state[n-1] = (i+1)%n
       go check_states(start_state, stop_state, end_chan)
       started_checks += 1
    }

    for i := 0; i < started_checks; i++ {
    	fmt.Println("Sprawdzam")
        invalid_states := <-end_chan
    	for _, invalid_state := range(invalid_states) {
        	fmt.Println("Stan ", invalid_state, " jest nieprawidłowy!")
    	}
	}
}
