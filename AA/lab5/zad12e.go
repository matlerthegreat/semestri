package main

import (
    "fmt"
	)

const n = 9
type Config = [n]int
type State struct {
    config Config
}

var valid_states []int

func check_valid(config Config) int {
    index := 0
    mult := 1
    for i := 0; i < n; i++ {
		index += config[i]*mult
		mult *= (n+1)
    }

    return valid_states[index]
}

func set_valid(config Config, depth int) {
    index := 0
    mult := 1
    for i := 0; i < n; i++ {
		index += config[i]*mult
		mult *= (n+1)
    }

    valid_states[index] = depth
}

func dijkstra(state State) int {
	valid_depth := check_valid(state.config)
    if valid_depth > 0 {
        return valid_depth
    }
    legal := true
    value := state.config[0]
    for i := 1; i < n; i++ {
        if (state.config[i] != value) {
            legal = false
			break
        }
    }
    if legal {
	    set_valid(state.config, 1)
        return 1
    }

	moved := 0
	for i := 0; i < n; i++ {
        if i == 0 {
            if (state.config[0] == state.config[n-1]) {
	            moved += 1
            } else {
                continue
            }
        } else {
            if (state.config[i] != state.config[i-1]) {
                moved += 1
            } else {
                continue
            }
        }
    }

    if (moved == 1) {
	    set_valid(state.config, 1)
	    return 1
    }

	max_depth := 0
    for i := 0; i < n; i++ {
        new_state := state
        if i == 0 {
            if (state.config[0] == state.config[n-1]) {
                new_state.config[0] = (state.config[0] + 1)%(n+1)
            } else {
                continue
            }
        } else {
            if (state.config[i] != state.config[i-1]) {
                new_state.config[i] = state.config[i-1]
            } else {
                continue
            }
        }

		new_depth := dijkstra(new_state)
		if (new_depth > max_depth) {
			max_depth = new_depth
		}
    }

    set_valid(state.config, max_depth + 1)
    return max_depth + 1
}

/*func check_state(config Config) uint64 {
    var states_to_check []State
    states_to_check = append(states_to_check, State{config, 1})
    var checked_configs []Config


	var max_depth uint64 = 0
    for len(states_to_check) > 0 {
        if states_to_check[len(states_to_check)-1].depth > max_depth {
            max_depth = states_to_check[len(states_to_check)-1].depth
        }
        next_states := dijkstra(states_to_check[len(states_to_check)-1])
        checked_configs = append(checked_configs, states_to_check[len(states_to_check)-1].config)
        states_to_check = states_to_check[:len(states_to_check)-1]
        for _, next_state := range(next_states) {
            states_to_check = append(states_to_check, next_state)
        }
    }

    for _, checked_config := range(checked_configs) {
        set_valid(checked_config)
    }

	return max_depth
}*/

func check_states(start_state Config, end_state Config, end_chan chan int) {
    state := start_state
    fmt.Println("od ", start_state, " do ", end_state)

	var max_depth int = 0
	for {
    	depth := dijkstra(State{state})
    	if depth > max_depth {
        	max_depth = depth
    	}

    	for i := 0; i < n; i++ {
        	if state[i] < n {
            	state[i] += 1
            	break
        	}
        	state[i] = 0
    	}

    	if state == end_state {
        	break
    	}
	}

	end_chan <- max_depth
}

func main() {
    end_chan := make(chan int)
    valid_states = make([]int, 10*10*10*10*10*10*10*10*10)
    /*for i := 0; i < 10*10*10*10*10*10*10*10*10; i++ {
	     valid_states[i] = 0
    }*/
    started_checks := 0
    for i := 0; i < n; i++ {
       var start_state Config
       var stop_state Config
       start_state[n-1] = i
       stop_state[n-1] = (i+1)%n
       go check_states(start_state, stop_state, end_chan)
       started_checks += 1
    }

	var max_depth int = 0
    for i := 0; i < started_checks; i++ {
    	fmt.Println("Sprawdzam")
        depth := <-end_chan
    	if depth > max_depth {
        	max_depth = depth
    	}
	}

	fmt.Println("Długość najdłuszej ścieżki wynosi ", max_depth - 1)
}
