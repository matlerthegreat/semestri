package main

import (
    "math/rand"
)

const n = 100

type Graph [n*n]bool
type State struct {
    graph Graph
    ind map[int]bool
}

func (g *Graph) is_connected(v int, w int) bool {
    return g[v*n+w]
}

func (g *Graph) set_connected(v int, w int) {
    g[v*n+w] = true
    g[w*n+v] = true
}


func gen_graph() Graph {
    var g Graph

    connected := false
    for !connected {
        v := rand.Intn(n)
        w := rand.Intn(n)
        if v == w {
            continue
        }
        g.set_connected(v, w)

        connected = true
        for v := 0; v < n; v++ {
            for w := 0; w < n; w++ {
                if v == w {
                    continue
                }
                if !g.is_connected(v, w) {
                    connected = false
                }
            }
        }
    }
    return g
}

func is_ind(g Graph, ind map[int]bool) bool {
    for v := 0; v < n; v++ {
        if !ind[v] {
            continue
        }
        for w := 0; w < n; w++ {
            if v == w || !ind[w] {
                continue
            }
            if g.is_connected(v, w) {
                return false
            }
        }
    }

    return true
}

func is_max_ind(g Graph, ind map[int]bool) bool {
    if !is_ind(g, ind) {
        return false
    }

    bigger_ind := make(map[int]bool)
    for k, v := range(ind) {
        bigger_ind[k] = v
    }
    for i := 0; i < n; i++ {
        if ind[i] {
            continue
        }

        bigger_ind[i] = true
        if is_ind(g, bigger_ind) {
            return false
        }
        bigger_ind[i] = false
    }

    return true
}

func process(v int, g Graph, ind map[int]bool) bool {
    all_zero := true
    for w := 0; w < n; w++ {
        if !g.is_connected(v, w) || v == w {
            continue
        }

        if ind[w] {
            all_zero = false
            break
        }
    }

    if all_zero && !ind[v] {
        ind[v] = true
        return true
    } else if !all_zero && ind[v]{
        ind[v] = false
        return true
    }

    return false
}

func step(state State) []State {
    if is_max_ind(state.graph, state.ind) {
        return nil
    }

    var next_states []State
    for v := 0; v < n; v++ {
        var new_state State
        new_state.graph = state.graph
        new_state.ind = make(map[int]bool)
        for k, v := range(state.ind) {
            new_state.ind[k] = v
        }

        if process(v, new_state.graph, new_state.ind) {
    		next_states = append(next_states, new_state)
        }
    }

    return next_states
}

func main() {
    g := gen_graph()
    ind := make(map[int]bool)

	var states_to_check []State
	states_to_check = append(states_to_check, State{g, ind})
	for len(states_to_check) > 0 {
    	l := len(states_to_check)
    	state_to_check := states_to_check[l-1]
    	states_to_check = states_to_check[:l-1]

    	next_states := step(state_to_check)
    	for _, next_state := range(next_states) {
        	states_to_check = append(states_to_check, next_state)
    	}
	}
}
