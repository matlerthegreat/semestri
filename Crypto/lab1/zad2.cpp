#include <iostream>
#include <array>
#include <bitset>
#include <cstdlib>

std::array<int, 32> r;

unsigned int predict(size_t n)
{
    r[n%32] = r[(n-31)%32] + r[(n-3)%32];
    return r[n%32] >> 1;
}

int main()
{
    srand(100);
    for (auto i = 0u; i < 32u; ++i)
        r[i] = rand();

    auto correct = 0u;
    auto sample_count = 100;
    for (auto i = 0u; i < sample_count; ++i) {
        auto expected = rand();
        auto predicted = predict(i);

        std::bitset<32> bits(expected ^ ~predicted);
        correct += bits.count();

        std::cerr << expected << ", " << predicted << std::endl;
        std::cout << "[" << i << "]: Correct ratio: " << static_cast<double>(correct)/(i+1)/32 << std::endl;
        if (predicted != expected) {
            r[i%32] = expected << 1;
        }
    }

}
