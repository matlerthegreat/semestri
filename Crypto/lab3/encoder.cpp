#include <iostream>
#include <random>
#include <vector>
#include <ranges>
#include <gmpxx.h>

int n = 8;

int main()
{
	std::vector<mpz_class> w{
		mpz_class("2"),
		mpz_class("7"),
		mpz_class("11"),
		mpz_class("21"),
		mpz_class("42"),
		mpz_class("89"),
		mpz_class("180"),
		mpz_class("354")
	};

	mpz_class sum;
	for (auto s : w)
		sum += s;

	mpz_class q("881");
	mpz_class r("588");
	mpz_class t("2");
	std::cout << (r*t)%q << std::endl;

	auto mul_mod = [r, q](mpz_class& x) -> mpz_class {return (x*r)%q;};
	const auto beta = w | std::views::transform(mul_mod);

    for (auto i : beta) {
        std::cout << i << ' ';
    }
    std::cout << std::endl;

    std::vector<bool> bits(8);
    bits[1] = 1;
    bits[2] = 1;
    bits[7] = 1;

    mpz_class c;
    for (auto i = 0; i < 8; ++i)
	    if (bits[i])
		    c += beta[i];

    std::cout << c << std::endl;

	return 0;
}
