#include <iostream>
#include <fstream>
#include <vector>
#include <ranges>
#include <gmp.h>
#include <gmpxx.h>
#include <ctime>

int range = 1000;

int main(int argc, const char* argv[])
{
	if (argc != 2) {
		std::cout << "Usage: " << argv[0] << " key length in bytes" << std::endl;
		return 1;
	}
	int n = std::stoi(argv[1])*8;
	gmp_randclass rand (gmp_randinit_default);
	rand.seed(time(0));
	mpz_class sum = 0;
	std::vector<mpz_class> w;

	for (auto i = 0; i < n; ++i) {
		mpz_class t = rand.get_z_range(range);
		t += sum;
		w.push_back(t);
		sum +=t;
	}
	std::reverse(std::begin(w), std::end(w));

	mpz_class q = sum + rand.get_z_range(range);
	mpz_class r;
	while (true) {
		mpz_class t = rand.get_z_range(q);
		if (gcd(t, q) == 1) {
			r = t;
			break;
		}
	}
	mpz_class r_inv;
	mpz_invert(r_inv.get_mpz_t(), r.get_mpz_t(), q.get_mpz_t());

	auto fout = std::ofstream("priv_key");
	fout << q << std::endl << r_inv << std::endl;
	for (auto i : w)
		fout << i << ' ';

	auto mul_mod = [r, q](mpz_class& x) -> mpz_class {return (x*r)%q;};
	const auto beta = w | std::views::transform(mul_mod);

	fout = std::ofstream("pub_key");
    for (auto i : beta) {
        fout << i << ' ';
    }

	return 0;
}
