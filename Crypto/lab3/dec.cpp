#include <iostream>
#include <fstream>
#include <vector>
#include <ranges>
#include <gmpxx.h>


int main()
{
	auto fin = std::ifstream("priv_key");
	mpz_class q, r;
	std::vector<mpz_class> w;

	fin >> q >> r;
	while(fin) {
		mpz_class t;
		fin >> t;
		w.push_back(t);
	}
	w.pop_back();


	mpz_class c; 
	fin = std::ifstream("ciphertext");
	fin >> c;

	mpz_class d = (c*r)%q;


	std::vector<bool> bits;
	auto i = 0;
	while (d > 0) {
		if (w[i] <= d) {
			d -= w[i];
			bits.push_back(true);
		}
		else
			bits.push_back(false);

		++i;
	}

	while (bits.size() % 8)
		bits.push_back(false);


	auto fout = std::ofstream("decrypted", std::ios::binary);
	for (auto i = 0; i < (bits.size()/8); ++i) {
		char t = 0;
		for (auto j = 0; j < 8; ++j) {
			t |= bits[i*8 + j] << j;
		}
		fout << t;
	}
	return 0;
}
