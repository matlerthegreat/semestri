#include <iostream>
#include <fstream>
#include <vector>
#include <ranges>
#include <gmpxx.h>

int main()
{
	auto fin = std::ifstream("pub_key");

	std::vector<mpz_class> beta;
	while(fin) {
		mpz_class t;
		fin >> t;
		beta.push_back(t);
	}
	beta.pop_back();


	fin = std::ifstream("plaintext", std::ios::binary);
    std::vector<bool> bits;
    while(fin && bits.size() <= beta.size()) {
	    char c = fin.get();
	    for (auto i = 0; i < 8; ++i) {
			bits.push_back(c & (1 << i));
	    }
    }


    mpz_class c;
    for (auto i = 0; i < bits.size(); ++i)
	    if (bits[i])
		    c += beta[i];
	auto fout = std::ofstream("ciphertext");


    fout << c;

	return 0;
}
