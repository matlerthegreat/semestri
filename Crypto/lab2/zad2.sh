#! /bin/bash

paste <(od -An -vtu1 -w1 message1) <(awk '{printf "%d", xor($1, $1+1)}' .iv) | awk '{printf "%c", xor($1, $2)}' > oracle_input

./zad1.lua oracle aes-128-cbc keystore oracle_input oracle_output

./zad1.lua challenge aes-128-cbc keystore message1 message2 challenge_output

cmp --silent oracle_output challenge_output && echo 'First message was encrypted' || echo 'Second message was encrypted'
