#! /bin/lua

mode = arg[2]
key_store_path = arg[3]
iv_file = io.open(".iv", "r")

if iv_file == nil then
    iv = 0
else
    iv = iv_file:read("*number")
end

iv_string = string.format("%x", iv)

if arg[1] == "oracle" then
    file_in = arg[4]
    file_out = arg[5]
    
    command = "openssl enc -a -nosalt -" .. mode .. " -iv " .. iv_string .. " -in " .. file_in .. " -out " .. file_out .. " -pass file:" .. key_store_path

    os.execute(command)
elseif arg[1] == "decrypt" then
    file_in = arg[4]
    file_out = arg[5]
    
    command = "openssl enc -d -a -nosalt -" .. mode .. " -iv " .. iv_string .. " -in " .. file_in .. " -out " .. file_out .. " -pass file:" .. key_store_path

    os.execute(command)
elseif arg[1] == "challenge" then
    file1_in = arg[4]
    file2_in = arg[5]
    file_out = arg[6]
    
    math.randomseed(os.time())
    if math.random() < 0.5 then
        print "Encrypting first message"
        command = "openssl enc -a -nosalt -" .. mode .. " -iv " .. iv_string .. " -in " .. file1_in .. " -out " .. file_out .. " -pass file:" .. key_store_path
    else
        print "Encrypting second message"
        command = "openssl enc -a -nosalt -" .. mode .. " -iv " .. iv_string .. " -in " .. file2_in .. " -out " .. file_out .. " -pass file:" .. key_store_path
    end
    
    os.execute(command)
end

iv_file = io.open(".iv", "w")
iv_file:write(iv+1)
