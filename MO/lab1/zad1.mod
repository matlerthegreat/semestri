param n, integer, >= 1;

param A{i in {1..n}, j in {1..n}}:=1/(i+j-1);

param b{i in {1..n}} := sum{j in {1..n}} 1/(i+j-1);
param c{i in {1..n}} := b[i];

var x{i in {1..n}} >= 0;

minimize Cost: sum{i in {1..n}} x[i] * b[i];

s.t. Condition{j in {1..n}} : sum{i in {1..n}} x[i]*A[i, j]=b[j];

solve;
display sqrt(sum{i in {1..n}}(x[i]-1)*(x[i]-1));

data;

param n := 10;
end;
