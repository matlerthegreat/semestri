
set Materials;
param MaterialsMinimumPurchase{m in Materials};
param MaterialsMaximumPurchase{m in Materials};
param MaterialsCost{m in Materials};

set Products;
param ProductsMinimumMaterials{p in Products, m in Materials};
param ProductsMaximumMaterials{p in Products, m in Materials};
param ProductsValue{p in Products};

set PrimaryProducts;
param MaterialsWasteRatio{p in PrimaryProducts, m in Materials};

param WasteCost{m in Materials, p in Products};

var MaterialsToBuy{m in Materials} >= MaterialsMinimumPurchase[m], <= MaterialsMaximumPurchase[m];
s.t. MinimumPurchase{m in Materials}: MaterialsToBuy[m] >= MaterialsMinimumPurchase[m];
s.t. MaximumPurchase{m in Materials}: MaterialsToBuy[m] <= MaterialsMaximumPurchase[m];

var MaterialsToUse{m in Materials, p in Products} >= 0;
s.t. NotToMuchUse{m in Materials}: (sum{p in Products} MaterialsToUse[m, p]) <= MaterialsToBuy[m];
s.t. ProductsMinimumRequirement{p in PrimaryProducts, m in Materials}: MaterialsToUse[m, p] >= ProductsMinimumMaterials[p, m] * (sum{n in Materials}MaterialsToUse[n, p]);
s.t. ProductsMaximumRequirement{p in PrimaryProducts, m in Materials}: MaterialsToUse[m, p] <= ProductsMaximumMaterials[p, m] * (sum{n in Materials}MaterialsToUse[n, p]);

var WasteToUse{m in Materials, p in PrimaryProducts} >= 0;
s.t. WasteMaximum{m in Materials, p in PrimaryProducts}: WasteToUse[m, p] <= MaterialsToUse[m, p] * MaterialsWasteRatio[p, m];
s.t. WasteForC: (sum{m in Materials} WasteToUse[m, 'A']) * 0.2 = 0.8 * MaterialsToUse[1, 'C'];
s.t. WasteForD: (sum{m in Materials} WasteToUse[m, 'B']) * 0.3 = 0.7 * MaterialsToUse[2, 'D'];


maximize Profit:
	(sum{p in PrimaryProducts}((sum{m in Materials}MaterialsToUse[m, p]*(1 - MaterialsWasteRatio[p, m]))*ProductsValue[p]))
	+ MaterialsToUse[1, 'C'] * ProductsValue['C'] / 0.2
	+ MaterialsToUse[2, 'D'] * ProductsValue['D'] / 0.3
    - (sum{m in Materials}MaterialsToBuy[m]*MaterialsCost[m])
	- (sum{p in PrimaryProducts, m in Materials}((MaterialsToUse[m, p]*MaterialsWasteRatio[p, m] - WasteToUse[m, p])*WasteCost[m, p]));

solve;

display Profit;
display MaterialsToBuy;
display MaterialsToUse;
display WasteToUse;
data;

set Materials := 1 2 3;
param MaterialsMinimumPurchase :=
	1	2000
	2	3000
	3	4000;
param MaterialsMaximumPurchase :=
	1	6000
	2	5000
	3	7000;
param MaterialsCost :=
	1	2.1
	2	1.6
	3	1.0;

set Products := 'A' 'B' 'C' 'D';
param ProductsMinimumMaterials
	:	1	2	3 :=
	'A'	0.2	0.4	0
	'B'	0.1	0	0;
param ProductsMaximumMaterials
	:	1	2	3 :=
	'A'	1	1	0.1
	'B'	1	1	0.3;
param ProductsValue :=
	'A'	3
	'B'	2.5
	'C'	0.6
	'D'	0.5;

set PrimaryProducts := 'A' 'B';
param MaterialsWasteRatio
	:	1	2	3 :=
	'A'	0.1	0.2	0.4
	'B'	0.2	0.2	0.5;

param WasteCost
	:	'A' 'B' :=
	1	0.1	0.05
	2	0.1	0.05
	3	0.2	0.4;
end;
