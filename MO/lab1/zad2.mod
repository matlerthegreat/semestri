set Cities;
param Distance{from in Cities, to in Cities};
param DeficitVip{c in Cities};
param DeficitStandard{c in Cities};
param SurplusVip{c in Cities};
param SurplusStandard{c in Cities};
param MoveCostStandard;
param MoveCostVip;

var MovementStandard{from in Cities, to in Cities} >= 0;
s.t. StandardMoveOutMaximum{from in Cities}: sum{to in Cities}MovementStandard[from, to] <= SurplusStandard[from];

var MovementVip{from in Cities, to in Cities} >= 0;
s.t. VipMoveOutMaximum{from in Cities}: sum{to in Cities}MovementVip[from, to] <= SurplusVip[from];

s.t. SatisfiedVip{to in Cities}: sum{from in Cities}MovementVip[from, to] >= DeficitVip[to];
s.t. SatisfiedStandard{to in Cities}: sum{from in Cities}(MovementStandard[from, to] + MovementVip[from, to]) = DeficitStandard[to] + DeficitVip[to];

minimize Cost: sum{from in Cities, to in Cities}(MovementStandard[from, to]*Distance[from, to]*MoveCostStandard + MovementVip[from, to]*Distance[from, to]*MoveCostVip);

solve;

display 'Przemieszczenie Standard';
display{from in Cities, to in Cities: MovementStandard[from, to] > 0}: MovementStandard[from, to];
display 'Przemieszczenie VIP';
display{from in Cities, to in Cities: MovementVip[from, to] > 0}: MovementVip[from, to];
display Cost;

data;

set Cities :=
	Warszawa
	Gdansk
	Szczecin
	Wroclaw
	Krakow
	Berlin
	Rostok
	Lipsk
	Praga
	Brno
	Bratyslawa
	Koszyce
	Budapeszt;

param DeficitStandard :=
    Warszawa	0
	Gdansk		20
	Szczecin	0
	Wroclaw		8
	Krakow		0
	Berlin		16
	Rostok		2
	Lipsk		3
	Praga		0
	Brno		9
	Bratyslawa	4
	Koszyce		4
	Budapeszt	8;
param DeficitVip :=
    Warszawa	4
	Gdansk		0
	Szczecin	0
	Wroclaw		0
	Krakow		8
	Berlin		4
	Rostok		0
	Lipsk		0
	Praga		4
	Brno		0
	Bratyslawa	0
	Koszyce		0
	Budapeszt	0;
param SurplusStandard :=
    Warszawa	14
	Gdansk		0
	Szczecin	12
	Wroclaw		0
	Krakow		10
	Berlin		0
	Rostok		0
	Lipsk		0
	Praga		10
	Brno		0
	Bratyslawa	0
	Koszyce		0
	Budapeszt	0;
param SurplusVip :=
    Warszawa	0
	Gdansk		2
	Szczecin	4
	Wroclaw		10
	Krakow		0
	Berlin		0
	Rostok		4
	Lipsk		10
	Praga		0
	Brno		2
	Bratyslawa	8
	Koszyce		4
	Budapeszt	4;
	
param Distance: Warszawa Gdansk Szczecin Wroclaw Krakow Berlin Rostok Lipsk Praga Brno Bratyslawa Koszyce Budapeszt :=
    Warszawa	0 284 454 301 252 518 629 603 518 459 533 391 545
    Gdansk		284 0 288 377 486 403 427 539 556 591 699 652 764
    Szczecin	454 288 0 309 527 127 177 276 373 493 615 703 733
    Wroclaw		301 377 309 0 236 295 470 326 217 215 330 402 427
    Krakow		252 486 527 236 0 531 698 552 394 259 297 177 293
    Berlin		518 403 127 295 531 0 195 149 282 433 553 697 689
    Rostok		629 427 177 470 698 195 0 306 474 627 748 871 880
    Lipsk		603 539 276 326 552 149 306 0 203 384 492 699 645
    Praga		518 556 373 217 394 282 474 203 0 185 290 516 443
    Brno		459 591 493 215 259 433 627 384 185 0 122 344 261
    Bratyslawa	533 699 615 330 297 553 748 492 290 122 0 313 162
    Koszyce		391 652 703 402 177 697 871 699 516 344 313 0 214
    Budapeszt	545 764 733 427 293 689 880 645 443 261 162 214 0;

param MoveCostStandard := 1;
param MoveCostVip := 1.15;

end;
