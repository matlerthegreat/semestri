%Autor: Mateusz Resterny
\documentclass[a4paper]{mwart}
\usepackage{polski}
\usepackage[polish]{babel}
\usepackage{indentfirst}
\usepackage{mathtools}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{url}
\brokenpenalty=1000
\clubpenalty=1000
\widowpenalty=1000
\frenchspacing
\begin{document}
\title{Metody optymalizacji\\Problem najdłuższego wspólnego podciągu}
\author{Mateusz Resterny}
\maketitle

\section{Wprowadzenie}
Informacja w dzisiejszym świecie stała się najcenniejszą walutą. Przyjmuje ona wiele postaci, od cyfrowych filmów przez tekst wiadomości mailowych, po dźwięk rozmowy telefonicznej. Nieustannie pozyskiwane są nowe dane w niewyobrażalnej ilości. Nie byłyby one jednak w żaden sposób przydatne, gdyby nie istniały wydajne sposoby na ich przetwarzanie, katalogowanie i wyciąganie z nich wniosków. W tym celu opracowane zostały standardowe formy zapisu informacji w postaci ciągu znaków. Alfabet, z którego pochodzą pojedyncze symbole może być dość abstrakcyjny i bogaty, jak np. zbiór gatunków zwierząt żyjących na Ziemi; lecz może też przyjąć postać bardzo sztywnego binarnego zapisu.

Niezależnie od konkretnej formy zapisu danych, podstawową operacją przeprowadzaną na zbiorach jest porównywanie ich elementów ze sobą. Częstym pytaniem jest: "jak bardzo różnią się od siebie dwa lub więcej ciągów", lub odwrotnie: "jak bardzo są do siebie podobne". Należałoby zatem wprowadzić miarę podobieństwa, która to wyznaczałaby które z części ciągów są takie same, a które nie posiadają swojego odpowiednika w pozostałych egzemplarzach. Jedną z odpowiedzi na te pytania, jest najdłuższy wspólny podciąg.

Problem wyznaczania najdłuższego wspólnego podciągu pojawia się w wielu sytuacjach, w tym również w kwestiach niezwiązanych bezpośrednio z informatyką \cite{Nowowiejski}:
\begin{enumerate}
    \item Automatyczne wyszukiwanie plagiatów utworów muzycznych. System wyłapujący potencjalną kradzież cudzej twórczości musi być na tyle elastyczny, by wykryć nie tylko dokładnych kopii, lecz przede wszystkim podejrzanie podobnych elementów w linii melodycznej.
    \item Proces scalania plików w przypadku wystąpienia konfliktów przy użyciu systemu kontroli wersji. Edycje w kodzie mogą pojawić się w wielu miejscach, a odpowiedni program powinien w przejrzysty sposób wskazać programistom, zarówno zmienione części kodu, jak i te pozostawione bez modyfikacji.
    \item Określanie pokrewieństwa organizmów poprzez porównanie ich sekwencji DNA. Kolejne pary kwasów w podwójnej helisie mogą zostać oddzielone od siebie, jednak ich wystąpienie powinno zostać mimo wszystko uchwycone.
\end{enumerate}

\section{Standardowe podejście do problemu}
Jedna z pierwszych prac zajmująca się problemem najdłuższego wspólnego podciągu pojawiła się w roku 1975 i dotyczyła znalezienia ,,odległości edycyjnej`` dwóch ciągów znaków. Wyznaczanie tej miary miało pomóc przy procedurach poprawiania błędów w pisowni, poprzez wyszukiwanie spośród listy znanych wyrazów, słowa do którego uzyskania wymagana jest najmniejsza liczba edycji\cite{Wagner}.

Nakreślone zostały w niej podstawowe techniki, które zostały ulepszane w późniejszym czasie. Przedstawiono również formalny zapis problemu --- dane są dwa skończone ciągi symboli: $A$ i $B$. $A<i>$ oznacza $i$-ty symbol w ciągu, natomiast $A \langle i:j \rangle$ wyznacza podciąg $A\langle i \rangle A \langle i+1 \rangle \dots A \langle j \rangle$. Przekształcenie ciągu $A$ w ciąg $B$ można uzyskać poprzez zastosowanie jednej z trzech podstawowych operacji na znakach ciągów: wstawienie ($\Lambda => b$), usuwanie ($a => \Lambda$) i zamianę ($a => b$), $\Lambda$ symbolizuje brak znaku.

Zaproponowane podejście opiera się na programowaniu dynamicznym. Algorytm wyznacza koszt edycji $C$ jednego ciągu w drugi. Funkcja działa na zasadzie obliczania szczątkowego kosztu zamiany ostatniego znaku każdego z dwóch podciągów $c$, iteracyjnie wyznaczając koszt edycji reszty ciągu, wybierając przy tym najbardziej korzystną operację.
\begin{algorithm}[H]
    \caption{Algorytm Wagnera-Fishera}
\begin{algorithmic}
    \STATE $C[0, 0] \leftarrow 0$
    \FOR{$i \in \{1, \dots, |A|\}$}
    	\STATE $C[i, 0] \leftarrow C[i-1, 0] + c(A \langle i \rangle => \Lambda)$
    \ENDFOR
    \FOR{$j \in \{1, \dots, |B|\}$}
    	\STATE $C[0, j] \leftarrow C[0, j - 1] + c(\Lambda => B \langle i \rangle)$
    \ENDFOR

    \FOR{$i \in \{1, \dots, |A|\}$}
        \FOR{$j \in \{1, \dots, |B|\}$}
        	\STATE $m_1 \leftarrow C[i - 1, j - 1] + c(A \langle i \rangle => B \langle i \rangle)$
        	\STATE $m_2 \leftarrow C[i - 1, j] + c(A \langle i \rangle => \Lambda)$
        	\STATE $m_1 \leftarrow C[i, j - 1] + c(\Lambda => B \langle i \rangle)$
        	\STATE $C[i, j] \leftarrow \min\{m_1, m_2, m_3\}$
        \ENDFOR
    \ENDFOR
\end{algorithmic}
\end{algorithm}

Łatwo zauważyć, że algorytm działa w czasie $O(|A| * |B|)$, wymagając również $O(|A| * |B|)$ pamięci. Autorzy publikacji wskazują, że ustalenie kosztu operacji usuwania i dodawania znaku na 1, a operacji zmiany znaku na inny na 2 skutkować będzie algorytmem znajdującym najdłuższy wspólny podciąg dwóch wejściowych łańcuchów.

\section{Niejawna tablica dla zmniejszenia pamięci}

W algorytmie przedstawionym w poprzednim rozdziale, wymagana jest tablica kosztów przekształcenia jednego ciągu w drugi. W artykule opublikowanym zaledwie rok po algorytmie Wagnera-Fishera, Hirschberg zauważył, że do obliczenia kolejnego rzędu tej macierzy, wymagany jest jedynie rząd poprzedni. Nie trzeba zatem przechowywać w pamięci całej tablicy, a jedynie ostatnio przetworzony rząd \cite{Hirschberg1}

\begin{algorithm}[H]
    \caption{Algorytm Hirschberga wyznaczenie ostatniego wiersza macierzy kosztów}
\begin{algorithmic}
    \FOR{$j \in \{1, \dots, |B|\}$}
    	\STATE $C[1, j] \leftarrow 0$
    \ENDFOR

    \FOR{$i \in \{1, \dots, |A|\}$}
    	\STATE $C[0, j] \leftarrow C[1, j]$
        \FOR{$j \in \{1, \dots, |B|\}$}
        	\IF{$A \langle i \rangle = B \langle i \rangle$}
            	\STATE $C[1, j] \leftarrow C[0, j-1] + 1$
			\ELSE
            	\STATE $C[1, j] \leftarrow \max\{C[1, j-1], C[0, j]\}$
            \ENDIF
        \ENDFOR
    \ENDFOR
\end{algorithmic}
\end{algorithm}

Podejście to sprawia jednak dość poważny problem, brak jawnej tablicy z całą historią obliczeń uniemożliwia łatwe odtworzenie najbardziej optymalnej ścieżki w celu uzyskania najdłuższego wspólnego podciągu. Z tego względu Hirschberg zaproponował podejście zgodne z zasadą "dziel i zwyciężaj", gdzie wejściowe łańcuchy dzielone są do momentu uzyskania trywialnych przypadków, a ich rezultaty scalane. Taka operacja przynosi dodatkowy koszt, jednak całkowite użycie pamięci zamyka się asymptotycznie w $O(m+n)$ utrzymując przy tym czas $O(m*n)$.

W kolejnych latach Hirschberg wypracował jeszcze lepsze algorytmy skupiając się tym razem na zmniejszeniu asymptotycznego czasu ich działania. Tym razem udało się wyznaczyć procedurę, której złożoność zależała od długości szukanego najdłuższego wspólnego podciągu. Utworzone algorytmy zajmowały $O(pn + n \log n)$ i $O((m+1-p)p \log n)$ dla ciągów wejściowych długości $m$ i $n$ oraz znajdującym wspólnym podciągu długości $p$ \cite{Hirschberg2}.

% Real-world example
\section{Dalsze optymalizacje dla realnego wykorzystania}

Wyprowadzone przez Hirschenberga algorytmy w szczególnych przypadkach schodzą do dość niskiej złożoności obliczeniowej, gdy wejściowe ciągi mają niewiele par wspólnych do $O(n \log n)$. Dużo częściej zdarza się jednak, że wejściowe ciągi są do siebie podobne, np przy porównywaniu dwóch wersji tego samego pliku przy użyciu narzędzia $diff$. Dla dość niedużych alfabetów, jakim jest alfabet łaciński, powtórzenia także dość pospolite.

Z tego powodu, James W. Hunt i Thomas G. Szymanski zaproponowali swoją wersję algorytmu działającą w czasie $O((r+n)\log n)$, wykorzystując przy tym $O(n+r)$ pamięci. Wynik ten osiągnęli usprawniając podstawową procedurę Wagnera poprzez wyznaczanie wyłącznie ,,istotnych'' wartości w macierzy przekształceń \cite{HuntSzymanski}.

\begin{algorithm}[H]
    \caption{Algorytm Hunta-Szymanskiego}
\begin{algorithmic}
    \STATE $THRESH[0] \leftarrow 0$
    \FOR{$j \in \{1, \dots, |B|\}$}
    	\STATE $THRESH[i] \leftarrow n+1$
    \ENDFOR

    \FOR{$i \in \{1, \dots, |A|\}$}
        \FOR{$j \in \{|B|, \dots, 1\}$}
        	\IF{$A \langle i \rangle = B \langle i \rangle$}
        		\STATE find $k$ such that $THRESH[k-1] < j \leq THRESH[k]$
            	\STATE $THRESH[i] \leftarrow j$
            \ENDIF
        \ENDFOR
    \ENDFOR
	\STATE find largest $k$ such that $THRESH[k] \neq n+1$
\end{algorithmic}
\end{algorithm}

Wydajność tego algorytmu wywodzi się z faktu, iż kolejne wartości w macierzy przekształceń nie maleją w każdym jej wierszu, zatem wyszukiwanie w tablicy $TRESH$ można zaimplementować wydajnie.

Autorzy zaznaczyli również, że w celu porównywania plików można posłużyć się operacją zwiększającą alfabet wejściowych ciągów, dzięki czemu średnia złożoność algorytmu zbijana jest zaledwie do $O(n \log n )$. Przyjmuje się, że alfabetem plików są ich poszczególne linie tekstu, a właściwie ich hashe. Przedstawiony algorytm używany jest w Unixowej implementacji narzędzia $diff$ \cite{diff}.

\section{Teoretyczne rozprawy o oczekiwanej długości najdłuższego wspólnego podciągu}

Jak pokazano w poprzednim rozdziale, znane algorytmy wyznaczające najdłuższy wspólny podciąg działają lepiej dla dużych alfabetów, gdy prawdopodobieństwo ,,przypadkowo'' pojawiających się wspólnych podciągów. Nie jest zaskakujące więc, że powstały prace badające średnią długość najdłuższych wspólnych podciągów dla różnych wejściowych łańcuchów znaków.

Wybierając losowo, z jednorodnego rozkładu dwa ciągi tej samej długości $n$ z alfabetu zawierającego $k$ znaków, wiadomo było, że stosunek spodziewanej długości najdłuższego wspólnego podciągu do długości łańcuchów wejściowych zbiegać będzie do do stałej: $\lim_{n \to \infty}\mathbf{E}(L)/n = \gamma_k$. Od lat 80. przypuszczano, że stała ta cechuje się własnością $\lim_{k \to \infty}\gamma_k \sqrt{k} \to 2$, co zostało potwierdzone dopiero w 2003. Dowód ten opierał się na innych pracach badających grafy dwudzielne --- o znakach z dwóch łańcuchów można myśleć jak o wierzchołkach, krawędzie natomiast łączą te same znaki z dwóch ciągów; oraz o spodziewaną długość najdłuższego podciągu rosnącego \cite{kiwi}.

Bardziej szczegółowe podejście do problemu zostało zaproponowane przez Georga Luekera, pozwalające obliczyć wartość $\gamma_k$ dla dowolnego $k$. Rozwiązanie to polega na programowaniu dynamicznym oraz wyliczeniach numerycznych \cite{lueker}.

\section{Praca nad zwrównolegleniem obliczeń}

Problem znalezienia najdłuższego wspólnego podciągu jest problemem $\mathbf{NP}$ ~--- trudnym. Zgodnie z aktualnie posiadaną przez ludzkość wiedzą zakładamy, że $\mathbf{P} \neq \mathbf{NP}$. Wynika stąd, iż prawdopodobnie nie istnieją wydajne algorytmy na odszukanie rozwiązania ogólnego przypadku w czasie wielomianowym. Gdyby znaleźć przynajmniej jeden, cała klasa nierozwiązywalnych dotychczas problemów okazać by się mogła rozstrzygnięta.

Ze względu na teoretyczne ograniczenia, jak i praktyczne wymagania, często najlepszym możliwym podejściem jest wykorzystanie procesów równoległych. Obecnie zatem, najwięcej badań dokonuje się właśnie w dziedzinie znalezienia rozszerzenia znanych już algorytmów do działania na wielu procesorach.

Przedstawione podejścia bazują na podstawowym algorytmie opartym na programowaniu dynamicznym. Należy jednak przypomnieć, że w trakcie jego działania, konstruowanie tablicy zawierającej najdłuższe podciągi kolejnych prefiksów dwóch wejściowych łańcuchów znaków przebiega komórka po komórce, podążając najpierw wzdłuż wiersza. Kolejne wartości wymagają wcześniejszego wyznaczenia poprzednich wierszy i kolumn. Z tego względu naiwne zrównoleglenie obliczeń nie ma prawa zadziałać.

Aby uniknąć ciągłego odwoływania się do uprzednio obliczonych wartości, bez kosztownych operacji synchronizacji pamięci, zaproponowano zmienione podejście obliczające je po przekątnej \cite{babu}. Takie rozwiązanie pozwala na łatwiejsze obliczenia, jednak wciąż występuje ciągła potrzeba synchronizacji.


Kolejnym przełomem w dziedzinie algorytmów równoległych dla obliczania najdłuższego wspólnego podciągu było wyprowadzenie procedury działającej rząd po rzędzie macierzy. Takie rozwiązanie pozwala obliczać cały rząd wartości na raz, równolegle przy wykorzystaniu wielu procesorów bez potrzeby nieustannej synchronizacji pamięci oraz bez ryzyka wystąpienia wyścigów \cite{yang}:

\[
R[i, j] = 
    \begin{cases}
        0 & : i=0 \lor j=0 \\
        R[i-1, j-1] + 1 & : A\langle i \rangle = b \langle i \rangle \\
        \max{R[i - 1, j], r[i-1, j-k-1]+1} & : A \langle i \rangle = B \langle j-k \rangle \\
        \max{R[i-1, j], 0} & : j-k=0
    \end{cases}
\]

\section{Podsumowanie}

Problem znalezienia najdłuższego wspólnego podciągu pojawia się w wielu dziedzinach, zatem powstała spora liczba algorytmów na jego rozwiązanie. Co ciekawe, znaczna większość badań ogranicza się jedynie do odnalezienia najdłuższego wspólnego podciągu dla jedynie dwóch łańcuchów wejściowych. Optymalizacja ogólnego przypadku okazuje się być bardzo trudna i nie znajduje zastosowania w szerokiej gamie realnych problemów.

Patrząc jedynie na przypadek dla dwóch łańcuchów wejściowych, tu również pojawiają się problemy w implementacji rozwiązań potrzebnych problemów. Alfabety, jakie pojawiają się naturalnie są zazwyczaj nieduże, w przypadku sekwencjonowania DNA to jedynie cztery litery dla czterech zasad azotowych (A - adenina, G - guanina, C - cytozyna, T tymina). Nawet alfabet łaciński jest stosunkowo niewielki. Z budowy algorytmów czas ich wykonania jest tym większy, im mniejszy jest alfabet, dla tego przy operacjach na tekście grupuje się go np. w linie i te traktowane są jako pojedyncze znaki znacznie większego alfabetu.

Wreszcie ostatnią szansą na osiągnięcie zadowalającego czasu działania jest zwrócenie się ku algorytmom równoległym. Ten powód oraz fakt olbrzymiego wzrostu liczby rdzeni procesorów dostępnych dla konsumentów sprawiają, że obecnie największy nacisk kładzie się właśnie w wyprowadzanie procesów najlepiej poddających się zrównolegleniu.

Warto zauważyć, że większość przedstawionych algorytmów bazuje bardzo mocno na jednym z pierwszych podejść zaproponowanym przez Wagnera i Fishera w 1974r. Wszystkie z nich konstruują macierz kosztu przekształcenia jednego ciągu w drugi, opierając się o programowanie dynamiczne i dzielenie problemu na mniejsze, trywialne elementy. Późniejsze odkrycia polegały w dużej mierze na optymalizacji, te bardziej przełomowe zostały przedstawione w powyższej pracy. Pozostałe skupiały się na znajdowaniu wyjątkowych przypadków, szukaniu niuansów do wycięcia niewielkiej części czasu obliczeniowego kosztem dużo bardziej skomplikowanych formuł.

\newpage
\bibliographystyle{ieeetr}
\bibliography{literatura}
\end{document}
