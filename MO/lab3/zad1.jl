# Autor: Mateusz Resterny
using JuMP
using GLPK
using DelimitedFiles

function solveProblem(machines_cout, jobs_count, costs, times, capacities)
    epsilon = 1.0f-5
	machines = collect(1:machines_cout)
	jobs = collect(1:jobs_count)
	original_machines = copy(machines)
	original_jobs = copy(jobs)
	original_capacities = copy(capacities)
	solution = [0 for i in machines, j in jobs]
	graph = [1 for i in machines, j in jobs]

	n = 0
	while (length(jobs) > 0)
		n += 1
		if (n > 100)
			error()
		end
        model = Model(GLPK.Optimizer)

        @variable(model, x[original_machines, jobs] >= 0)
    	for j in jobs
        	@constraint(model, sum(x[i, j] for i in original_machines if graph[i, j] > 0) >= 1-epsilon)
        	@constraint(model, sum(x[i, j] for i in original_machines if graph[i, j] > 0) <= 1+epsilon)
    	end
    	for i in machines
        	@constraint(model, sum(times[i, j] * x[i, j] for j in jobs if graph[i, j] > 0) <= capacities[i])
        end

        @objective(model, Min, sum(costs[i, j] * x[i, j] for i in original_machines, j in jobs if graph[i, j] > 0))
        optimize!(model)

		for i in original_machines
			for j in jobs
    			if (value.(x)[i, j] <= epsilon)
    				graph[i, j] = 0
				end
    		end
		end
		for i in original_machines
			for j in jobs
				if ((value.(x)[i, j] >= 1-epsilon) && (value.(x)[i, j] <= 1+epsilon))
					solution[i, j] = 1
					deleteat!(jobs, findfirst(isequal(j), jobs))
					capacities[i] -= times[i, j]
				end
			end
		end
		for i in machines
			connections = [value.(x)[i, j] for j in jobs if graph[i, j] > 0]
			if (length(connections) == 1)
				deleteat!(machines, findfirst(isequal(i), machines))
			elseif ((length(connections) == 2) && (sum(connections) >= 1-epsilon))
				deleteat!(machines, findfirst(isequal(i), machines))
			end
		end
	end


	used_times = [sum(solution[i, j]*times[i, j] for j in original_jobs) for i in original_machines]
	mse = (1/machines_cout)*sum(used_times[i]/original_capacities[i] for i in original_machines)
	m = maximum(used_times[i]/original_capacities[i] for i in original_machines)

	print("$n, $mse, $m\n")
end

open(ARGS[1]) do file
	s = read(file, String)
	data = split(s)
	numbers = map(x->parse(Int, x), data)
	l = 1
	problems_count = numbers[l]
	l += 1

	print("n, mse, max\n")

    for problem in 1:problems_count
    	machines_cout = numbers[l]
		l += 1
    	jobs_count = numbers[l]
		l += 1
    	costs = [numbers[l + (i-1)*jobs_count+(j-1)] for i in 1:machines_cout, j in 1:jobs_count]
    	l += machines_cout*jobs_count
    	times = [numbers[l + (i-1)*jobs_count+(j-1)] for i in 1:machines_cout, j in 1:jobs_count]
    	l += machines_cout*jobs_count
    	capacities = [numbers[l + (i-1)] for i in 1:machines_cout]
    	l += machines_cout
		try
        	solveProblem(machines_cout, jobs_count, costs, times, capacities)
		catch e
			print("$e\n")
		end
    end
end

