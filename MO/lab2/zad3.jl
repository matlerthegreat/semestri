# Autor: Mateusz Resterny
using JuMP
using GLPK

# Data section
n = 4

a = Vector(undef, n)
b = Vector(undef, n)
c = Vector(undef, n)

a[1] = 2
a[2] = 1
a[3] = 1
a[4] = 2

b[1] = 2
b[2] = 1
b[3] = 1
b[4] = 1

c[1] = 2
c[2] = 2
c[3] = 2
c[4] = 1

# Model section
model = Model(GLPK.Optimizer)
@variable(model, x[1:n, 1:n], Bin)

@variable(model, timeStartA[1:n] >= 0)
@variable(model, timeStartB[1:n] >= 0)
@variable(model, timeStartC[1:n] >= 0)

for i in 2:n
	@constraint(model, timeStartA[i] >= timeStartA[i-1] + sum(x[i-1, j]*a[j] for j in 1:n))
	@constraint(model, timeStartB[i] >= timeStartB[i-1] + sum(x[i-1, j]*b[j] for j in 1:n))
	@constraint(model, timeStartC[i] >= timeStartC[i-1] + sum(x[i-1, j]*c[j] for j in 1:n))
end

for i in 1:n
	@constraint(model, timeStartB[i] >= timeStartA[i] + sum(x[i, j]*a[j] for j in 1:n))
	@constraint(model, timeStartC[i] >= timeStartB[i] + sum(x[i, j]*b[j] for j in 1:n))
end

#Minimize the time
@objective(model, Min, timeStartC[n] + sum(x[n, j]*c[j] for j in 1:n))
#Make sure that all processes are excecuted
@constraint(model, ones(n) .<= [sum([x[i, j] for j in 1:n]) for i in 1:n] .<= ones(n))
@constraint(model, ones(n) .<= [sum([x[i, j] for i in 1:n]) for j in 1:n] .<= ones(n))


optimize!(model)

optimal_variables = [(i, j) for j in 1:n, i in 1:n if value.(x)[i, j] > 0]
optimal_value = objective_value(model)
optimal_map = Dict()
for (i, j) in optimal_variables
	optimal_map[i] = j
end

println("Łączny czas wykonywania programu to $optimal_value")
println("Procesor A:")
for i in 1:n
	process = optimal_map[i]
	start = value.(timeStartA)[i]
	stop = value.(timeStartA)[i] + a[process]
	println("Porces $process ($start => $stop)")
end
println("Procesor B:")
for i in 1:n
	process = optimal_map[i]
	start = value.(timeStartB)[i]
	stop = value.(timeStartB)[i] + b[process]
	println("Porces $process ($start => $stop)")
end
println("Procesor C:")
for i in 1:n
	process = optimal_map[i]
	start = value.(timeStartC)[i]
	stop = value.(timeStartC)[i] + c[process]
	println("Porces $process ($start => $stop)")
end
