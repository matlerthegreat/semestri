# Autor: Mateusz Resterny
using JuMP
using GLPK

# Data section
n = 4

a = Vector(undef, n)
b = Vector(undef, n)
c = Vector(undef, n)

a[3] = 1
a[2] = 1
a[4] = 2
a[1] = 2

b[3] = 1
b[2] = 1
b[4] = 1
b[1] = 2

c[3] = 2
c[2] = 2
c[4] = 1
c[1] = 2

# Model section
model = Model(GLPK.Optimizer)
@variable(model, x[1:n, 1:n], Bin)

@variable(model, timeStartA[1:n] >= 0)
@variable(model, timeStartB[1:n] >= 0)
@variable(model, timeStartC[1:n] >= 0)

for i in 2:n
	@constraint(model, timeStartA[i] >= timeStartA[i-1] + sum(x[i, j]*a[j] for j in 1:n))
	@constraint(model, timeStartB[i] >= timeStartB[i-1] + sum(x[i, j]*b[j] for j in 1:n))
	@constraint(model, timeStartC[i] >= timeStartC[i-1] + sum(x[i, j]*c[j] for j in 1:n))
end

for i in 1:n
	@constraint(model, timeStartB[i] >= timeStartA[i] + sum(x[i, j]*a[j] for j in 1:n))
	@constraint(model, timeStartC[i] >= timeStartB[i] + sum(x[i, j]*b[j] for j in 1:n))
end

#Minimize the time
@objective(model, Min, timeStartC[n] + sum(x[n, j]*c[j] for j in 1:n))
#Make sure that not too much memory is used
@constraint(model, tooFew, ones(n) .<= [sum([x[i, j] for j in 1:n]) for i in 1:n] .<= ones(n))
#Make sure that all requred functions are computed
@constraint(model, tooMany, ones(n) .<= [sum([x[i, j] for i in 1:n]) for j in 1:n] .<= ones(n))


optimize!(model)

optimal_variables = [(i, j) for i in 1:n, j in 1:n if value.(x)[i, j] > 0]
optimal_value = objective_value(model)

show(value.(x))
println("Łączny czas wykonywania programu to $optimal_value")
for (i, j) in optimal_variables
	println("$i: uruchomić zadanie $j")
end
