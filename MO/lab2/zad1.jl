# Autor: Mateusz Resterny
using JuMP
using GLPK

# Data section
n = 3
m = 2
t = Vector(undef, n)
q = Matrix(undef, m, n)

t[1] = 3
t[2] = 10
t[3] = 6

q[1, 1] = 1
q[2, 1] = 0

q[1, 2] = 1
q[2, 2] = 1

q[1, 3] = 0
q[2, 3] = 1

# Model section
model = Model(GLPK.Optimizer)
@variable(model, x[1:n], Bin)

@objective(model, Min, sum([x[j]*t[j] for j in 1:n]))
@constraint(model, con, [sum([x[j]*q[i, j] for j in 1:n]) for i in 1:m] .>= ones(m))

optimize!(model)

optimal_variables = [j for j in 1:n if value.(x)[j] > 0]
optimal_value = objective_value(model)

println("Miejsca do przeszukania to $optimal_variables")
println("Łączny czas przeszukiwania to $optimal_value")
