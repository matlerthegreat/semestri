# Autor: Mateusz Resterny
using JuMP
using GLPK

# Data section
m = 3
n = 3
M = 20

I = Vector(undef, m)
r = Matrix(undef, m, n)
t = Matrix(undef, m, n)

I[1] = 1
I[2] = 1
I[3] = 0

r[1, 1] = 19
r[2, 1] = 19
r[3, 1] = 19

r[1, 2] = 10
r[2, 2] = 5
r[3, 2] = 10

r[1, 3] = 5
r[2, 3] = 10
r[3, 3] = 10

t[1, 1] = 1
t[2, 1] = 1
t[3, 1] = 1

t[1, 2] = 5
t[2, 2] = 10
t[3, 2] = 3

t[1, 3] = 5
t[2, 3] = 10
t[3, 3] = 3

# Model section
model = Model(GLPK.Optimizer)
@variable(model, x[1:m, 1:n], Bin)

#Minimize the time
@objective(model, Min, sum([x[i, j]*t[i, j] for i in 1:m, j in 1:n]))
#Make sure that all requred functions are computed
@constraint(model, [sum([x[i, j] for j in 1:n]) for i in 1:m] .>= I)
#Make sure that not too much memory is used
@constraint(model, M >= sum([x[i, j]*r[i, j] for i in 1:m, j in 1:n]))

optimize!(model)

optimal_variables = [(i, j) for i in 1:m, j in 1:n if value.(x)[i, j] > 0]
optimal_value = objective_value(model)

println("Łączny czas wykonywania programu to $optimal_value")
for (i, j) in optimal_variables
	println("Do obliczenia funkcji $i należy użyć podprogramu z $j")
end
